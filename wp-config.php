<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'admin_db' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'admin_user' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'password' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'MCcee4[Ie@z7!n%wPtGwbsY&,yTX/II=M>`)ab^|i[vIj?a:[TkB#`m{JsLA]L@!' );
define( 'SECURE_AUTH_KEY',  '_RqgYClczC{XK`pf7ztO@&A.eET&Y`Wnj*HHd_3g3w42IK`C94%)gURF5U/?J{MQ' );
define( 'LOGGED_IN_KEY',    '6_NHT:@#<+?=[T6#b`(5RV4D.y>@k:)R3Iaf*fy+[vvupK{j%.#_ bF$}7{s8<5t' );
define( 'NONCE_KEY',        'qj!yifzMMQ~/2!A#VQ)7IWEQ&>3A8PleCc`w]]s$+M/m;?;W>o[JP|2]:k2jGlyy' );
define( 'AUTH_SALT',        'm]vEE8p)LH{=aZZ-kB[aJN?tNkvY4hY=?H=&Q]`^Ky?dm%BCvjSfF3WT$Ddt;1J/' );
define( 'SECURE_AUTH_SALT', 'tWJJiHT{R7tkHc<9kiP*In@~uZk)sx=4Jl]>(u@cJG&m)e*I_u_+T9;Tq<h+{_AY' );
define( 'LOGGED_IN_SALT',   ')/2m[ XF0>,a%qM#`7FOg1%d~<_ZcO;Y(NoVdflY,>u}QGHm{!$J!Bp.Ye4/X&{G' );
define( 'NONCE_SALT',       'zpQ(~N)?BPWpRV^-i6B7=E4.-+{)x3h{EbAkV5ZXB~}j7B>9A7f)kTJp}P))4_I2' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );

define(‘FS_METHOD’, ‘direct’);
